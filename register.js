const lookup_country = require('./library/lookup/lookup_country');
const lookup_mcrta7p = require('./library/lookup/lookup_mcrta7p');
const lookup_mpotf1p = require('./library/lookup/lookup_mpotf1p');
const lookup_mvm01p = require('./library/lookup/lookup_mvm01p');

const insert_mcrta7p = require('./library/updatedata/insert_mcrta7p');
const insert_mpotf1p = require('./library/updatedata/insert_mpotf1p');
const insert_mvm01p = require('./library/updatedata/insert_mvm01p');
const insert_mvm02p = require('./library/updatedata/insert_mvm02p');
const update_mpotf1p = require('./library/updatedata/update_mpotf1p');
const upsert_mvm02p = require('./library/updatedata/upsert_mvm02p');

const genmb = require('./library/genmb');
const datetime = require('./library/datetime');

const create_new_card = require('./library/create_new_card');
const create_new_customer = require('.library/create_new_customer');
const schema = require('./library/checkSchema');

app.listen(8121, function () {
    console.log('app listening on port 8121!');
})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.post('/register', async function (req, res) {

    var dtf = await datetime.getdatetime();

    var custid = '';
    var mb = '';
    var ctry = '';
    var mb_type = 'MC';
    var mbnat_ = '';
    var nlnt_ = '';
    var card_rng = '';
    var mb_agen = '';
    //var MBHTEL_ = ''; //if (MBHTEL_ == '027777777' || MBPTEL_ == '027777777') {  console.log('Update PM110MP');
    //var MBPTEL_ = '';
    // create new card?
    var create_new = '';



    console.log('Validate Schema');
    if (req.body.DEMO_NTNL == 'TH') {
        if (checkID(req.body.CUST_ID)) {
            citizen = req.body.CUST_ID;
        }
    } else if (!req.body.DEMO_NTNL) {
        res.json({
            "RESP_CDE": 401,
            "RESP_MSG": "Missing Required Field : DEMO_NTNL"
        });
        return;
    } else if (req.body.CUST_ID.length > 10) {
        res.json({
            "RESP_CDE": 402,
            "RESP_MSG": "Invalid format : CUST_ID"
        });
        return;
    } else {

        // DEMO_NTNL != 'TH'
        let country_name = await lookup_country.lookup(req, res, req.body.DEMO_NTNL);
        //country_name = JSON.parse(result);
        ctry = country_name[0].CNTRYCD3;
        mbnat_ = country_name[0].MBNAT;
        if (req.body.DEMO_NTNL == 'TH') {
            custid = req.body.CUST_ID
        } else {
            custid = country_name[0].CNTRYCD3 + req.body.CUST_ID;
        }
        console.log(custid);
    }


    ///////////validate schema/////////////////
    // check schema
    schema.checkSchema(req, res, "REGISTER", dtf);



    let lookupmember_mcrta7p = await lookup_mcrta7p.lookup(req, res, custid, req.body.DEMO_TH_NAME, req.body.DEMO_TH_SURNAME);
    ////////////// exist customer ///////////////////
    if (lookupmember_mcrta7p == true) {
        let lookupmember_mvm01p = await lookup_mvm01p.lookup(req, res, custid, req.body.CUST_ID);
        ////////////// exist card //////////////////
        if (lookupmember_mvm01p.length != 0) {
            ///////// create more card //////////
            if (create_new == true) {
                let create_new_card = await create_new_card();
                if (create_new_card == true) {
                    res.json({
                        "RESP_SYSCDE": "",
                        "RESP_DATETIME": date_str,
                        "RESP_CDE": 102,
                        "RESP_MSG": "Success, found many MCard (already have cust_id)",
                        "MCARD_NUM": mb,
                        "CARD_TYPE": mb_type,
                        "CARD_EXPIRY_DATE": "999912",
                        "CARD_POINT_BALANCE": 0,
                        "CARD_POINT_EXPIRY": "999912",
                        "CARD_POINT_EXP_DATE": "999912"
                    });
                }
                ///////// not create more card ////////////
            } else {
                res.json({
                    "RESP_SYSCDE": "",
                    "RESP_DATETIME": date_str,
                    "RESP_CDE": 103,
                    "RESP_MSG": "You already have a card ",
                    "MCARD_NUM": mb,
                    "CARD_TYPE": mb_type,
                    "CARD_EXPIRY_DATE": "999912",
                    "CARD_POINT_BALANCE": 0,
                    "CARD_POINT_EXPIRY": "999912",
                    "CARD_POINT_EXP_DATE": "999912"
                });

            }

        }
        ///////////////////// not exist card ///////////////////////
        else {
            ////////////////////////don't forget parameter/////////////////////////////
            let create_new_card = await create_new_card(req, res, card_rng, mb, mbnat_, ctry, mb_agen, mb_type);
            if (create_new_card == true) {
                res.json({
                    "RESP_SYSCDE": "",
                    "RESP_DATETIME": date_str,
                    "RESP_CDE": 102,
                    "RESP_MSG": "Success, found many MCard (already have cust_id)",
                    "MCARD_NUM": mb,
                    "CARD_TYPE": mb_type,
                    "CARD_EXPIRY_DATE": "999912",
                    "CARD_POINT_BALANCE": 0,
                    "CARD_POINT_EXPIRY": "999912",
                    "CARD_POINT_EXP_DATE": "999912"
                });
            }
        }



        ////////////// not exist customer ///////////////////////
    } else {
        let lookupmember_mvm01p = await lookup_mvm01p.lookup(req, res, custid, req.body.CUST_ID);
        ////////////////// exist card //////////////////////
        if (lookupmember_mvm01p.length == 0) {
            ////////////// create more card ////////////////
            if (create_new == true) {
                ////////////////////////don't forget parameter////////////////////////
                let create_new_customer = await create_new_customer(req, res, card_rng,custid, mb, mbnat_, ctry, mb_agen, mb_type);
                if (create_new_customer == true) {
                    res.json({
                        "RESP_SYSCDE": "",
                        "RESP_DATETIME": date_str,
                        "RESP_CDE": 101,
                        "RESP_MSG": "Success",
                        "MCARD_NUM": mb,
                        "CARD_TYPE": "MC",
                        "CARD_EXPIRY_DATE": "999912",
                        "CARD_POINT_BALANCE": 0,
                        "CARD_POINT_EXPIRY": "999912",
                        "CARD_POINT_EXP_DATE": "999912"
                    });
                }
                ////////////// not create mor card //////////////////////
            } else {
                let insertmember_mcrta7p = await insert_mcrta7p(req, res, custid, req.body.DEMO_TH_NAME, req.body.DEMO_TH_SURNAME);
                if (insertmember_mcrta7p) {
                    res.json({
                        "RESP_SYSCDE": "",
                        "RESP_DATETIME": date_str,
                        "RESP_CDE": 103,
                        "RESP_MSG": "You already have a card ",
                        "MCARD_NUM": mb,
                        "CARD_TYPE": mb_type,
                        "CARD_EXPIRY_DATE": "999912",
                        "CARD_POINT_BALANCE": 0,
                        "CARD_POINT_EXPIRY": "999912",
                        "CARD_POINT_EXP_DATE": "999912"
                    });
                }
            }
        }
        /////////////// not exist card ///////////////////////
        else {
            ////////////////////////don't forget parameter////////////////////////
            let create_new_customer = await create_new_customer(req, res, card_rng,custid, mb, mbnat_, ctry, mb_agen, mb_type);
            if (create_new_customer == true) {
                res.json({
                    "RESP_SYSCDE": "",
                    "RESP_DATETIME": date_str,
                    "RESP_CDE": 101,
                    "RESP_MSG": "Success",
                    "MCARD_NUM": mb,
                    "CARD_TYPE": "MC",
                    "CARD_EXPIRY_DATE": "999912",
                    "CARD_POINT_BALANCE": 0,
                    "CARD_POINT_EXPIRY": "999912",
                    "CARD_POINT_EXP_DATE": "999912"
                });
            }
        }
    }
})

async function create_new_card(req, res, card_rng, mb, mbnat_, ctry, mb_agen, mb_type) {
    let genmb = await genmb.creatembcode(req,res,card_rng);
    console.log('Running number : ' + result.MBCODE_R);
    mb = genmb.MBCODE_R;
    let insertmember_mvm01p = await insert_mvm01p.insert(req, res, mb, mbnat_, ctry, mb_agen, mb_type);
    if (insertmember_mvm01p == true) {
        let upsertmember_mvm02p = await upsert_mvm02p.upsert(req, res, mb);
        if (upsertmember_mvm02p == true) {
            // let insertmember_mvm03p = await insert_mvm01p.insert(req, res, mb ,mbnat_  ,ctry , mb_agen, mb_type);
            if (insertmember_mvm03p == true) {
                let lookup_mpotf1p = await lookup_mpotf1p.lookup(req, res, mb);
                if (lookup_mpotf1p.length == 0) {
                    let insertmember_mpotf1p = await insert_mpotf1p.insert(req, res, mb);
                    return true;
                } else {
                    let updatemember_mpotf1p = await update_mpotf1p.update(req, res, mb);
                    return true;
                }
            }
        }
    }
}

async function create_new_customer(req, res, card_rng,custid, mb, mbnat_, ctry, mb_agen, mb_type) {
    let genmb = await genmb.creatembcode(req, res, card_rng);
    console.log('Running number : ' + result.MBCODE_R);
    mb = genmb.MBCODE_R;
    let insertmember_mcrta7p = await insert_mcrta7p.insert(req, res, custid, req.body.DEMO_TH_NAME, req.body.DEMO_TH_SURNAME);
    if (insertmember_mcrta7p == true) {
        let insertmember_mvm01p = await insert_mvm01p.insert(req, res, mb, mbnat_, ctry, mb_agen, mb_type);
        if (insertmember_mvm01p == true) {
            let upsertmember_mvm02p = await upsert_mvm02p.upsert(req, res, mb);
            if (upsertmember_mvm02p == true) {
                // let insertmember_mvm03p = await insert_mvm01p.insert(req, res, mb ,mbnat_  ,ctry , mb_agen, mb_type);
                if (insertmember_mvm03p == true) {
                    let lookup_mpotf1p = await lookup_mpotf1p.lookup(req, res, mb);
                    if (lookup_mpotf1p.length == 0) {
                        let insertmember_mpotf1p = await insert_mpotf1p.insert(req, res, mb);
                        return true;
                    } else {
                        let updatemember_mpotf1p = await update_mpotf1p.update(req, res, mb);
                        return true;
                    }
                }
            }
        }
    }
}